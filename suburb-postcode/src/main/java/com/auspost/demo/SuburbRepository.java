package com.auspost.demo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SuburbRepository extends CrudRepository<Suburb, Long> {

	@Transactional
    List<Suburb> findBySuburbName(String suburbName);
	@Transactional
	List<Suburb> findByPostCode(String postCode);
 
    
}
