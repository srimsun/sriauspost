package com.auspost.demo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AusPostService {
	@Autowired
	SuburbRepository subRep;
	
	public AusPostService() {
		super();
		// TODO Auto-generated constructor stub

	}


	
	public List<Suburb> findByPostCode(String postCode) {

		return subRep.findByPostCode(postCode);
	
		
	}

	public List<Suburb> getAllSuburbs(){

		return  (List<Suburb>) subRep.findAll();
	}
	
	public List<Suburb> findBySuburbName(String suburbName) {
		return subRep.findBySuburbName(suburbName);
		
	}
	
	public Suburb saveSuburb(Suburb suburb) {
		return subRep.save(suburb);
	}

		
	
}
