package com.auspost.demo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
	class DBLoad {

		@Bean
		CommandLineRunner initDatabase(SuburbRepository repository) {
			return args -> {
				repository.save(new Suburb("Melbourne", "3000"));
				repository.save(new Suburb("Altona Meadows", "3028"));
				
				repository.save(new Suburb("Derrimut", "3030"));
				repository.save(new Suburb("Footscray", "3011"));
				
				repository.save(new Suburb("Werribee", "3000"));
				repository.save(new Suburb("South Melbourne", "3205"));
			};
		}
	}

