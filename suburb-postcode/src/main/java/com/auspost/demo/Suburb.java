package com.auspost.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Suburb {


    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private  int id;
    private  String suburbName;
    private  String postCode;
    
    public Suburb() {
    	
    }
    
	public Suburb(int i, String suburbName, String postCode) {
		super();
		this.id = i;
		this.suburbName = suburbName;
		this.postCode = postCode;
	}

	

	public Suburb(String suburbName, String postCode) {
		super();
		this.suburbName = suburbName;
		this.postCode = postCode;
	}




	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSuburbName() {
		return suburbName;
	}



	public void setSuburbName(String suburbName) {
		this.suburbName = suburbName;
	}



	public String getPostCode() {
		return postCode;
	}



	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	
	
	


   
}
