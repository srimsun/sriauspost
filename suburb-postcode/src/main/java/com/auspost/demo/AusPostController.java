package com.auspost.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AusPostController {


@Autowired
AusPostService service;


    @RequestMapping("suburbs/postcode/{postCode}")
    @ResponseBody
    public List<Suburb> getSuburbs(@PathVariable(value = "postCode") String postCode) {
    	return service.findByPostCode(postCode);
    }
    
    @RequestMapping("/suburbs")
    public List<Suburb> getAllSuburbs() {

    	return service.getAllSuburbs();
    }
    @RequestMapping("/suburbs/suburbname/{suburbName}")
    @ResponseBody
    public List<Suburb> getAllSuburbs(@PathVariable(value = "suburbName") String suburbName) {

    	return service.findBySuburbName(suburbName);
    }
    
    @PostMapping("/suburbs/newsuburb")
	void newSuburb(@RequestBody Suburb suburb) {
		 service.saveSuburb(suburb);
	}
    
    @PostMapping("/suburbs")
	Suburb newEmployee(@RequestBody Suburb newSuburb) {
		return service.saveSuburb(newSuburb);
	}
    
    
    @PutMapping("/suburbs/update/{postCode}")
	Suburb updateSuburb(@RequestBody Suburb newSuburb, @PathVariable String postCode) {
		Suburb currSub = service.findByPostCode(postCode).get(0);
		
		if(currSub == null) {
			currSub= newSuburb;
		}
		else {
		currSub.setSuburbName(newSuburb.getSuburbName());
		currSub.setPostCode(newSuburb.getPostCode());
		}
		return service.saveSuburb(currSub);
		
	}
    @PostMapping ("/suburbs/add")
	Suburb addSuburb(@RequestBody Suburb newSuburb) {
		
		return service.saveSuburb(newSuburb);
		
	}
	
}
