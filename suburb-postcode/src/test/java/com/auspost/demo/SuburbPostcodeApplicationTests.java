package com.auspost.demo;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SuburbPostcodeApplicationTests {

    @Autowired
    private SuburbRepository suburbRepo;
    
    @Autowired
    private AusPostService service;

   
    
    @Test
    public void testFindBySuburbName() {
    	Suburb suburb = new Suburb("Altona Meadows","3028");
        suburbRepo.save(suburb);

        List<Suburb> findBySuburbName = suburbRepo.findBySuburbName(suburb.getSuburbName());
     
        Suburb sub = findBySuburbName.get(0);
        assertNotNull(sub);
        assertTrue( sub.getSuburbName().equalsIgnoreCase(suburb.getSuburbName()));
        assertTrue( sub.getPostCode().equalsIgnoreCase(suburb.getPostCode()));
       
    
    }
	
    @Test
    public void testFindByPostCode() {
    	Suburb suburb = new Suburb("Altona Meadows","3028");
        suburbRepo.save(suburb);

        List<Suburb> findBySuburbCode = suburbRepo.findByPostCode(suburb.getPostCode());
     
        Suburb sub = findBySuburbCode.get(0);
        assertNotNull(sub);
        assertTrue( sub.getSuburbName().equalsIgnoreCase(suburb.getSuburbName()));
        assertTrue( sub.getPostCode().equalsIgnoreCase(suburb.getPostCode()));
        
    }
    @Test
    public void testUpdatePostCode() {
    	Suburb suburb = new Suburb("Altona Meadows","3028");
        suburbRepo.save(suburb);

        List<Suburb> findBySuburbCode = suburbRepo.findByPostCode(suburb.getPostCode());
     
        Suburb sub = findBySuburbCode.get(0);
        assertNotNull(sub);
        assertTrue( sub.getSuburbName().equalsIgnoreCase(suburb.getSuburbName()));
        assertTrue( sub.getPostCode().equalsIgnoreCase(suburb.getPostCode()));
        sub.setPostCode("3000");
        suburbRepo.save(sub);;
        List<Suburb> findByUpdatedSuburbCode = suburbRepo.findByPostCode(sub.getPostCode());
        assertNotNull(findByUpdatedSuburbCode);
        Suburb updatedSuburb = findBySuburbCode.get(0);
        assertNotNull(updatedSuburb);
        assertTrue(updatedSuburb.getPostCode().equalsIgnoreCase("3000"));
       
    
    }
    
    
  
}
